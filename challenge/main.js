const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// global varible to store math operation
let result;

function sum(num1, num2) {
    return num1 + num2;
}

const substraction = (num1, num2) => num1 - num2;

function multiple(num1, num2) {
    return num1 * num2;
}

function divided(num1, num2) {
    return num1 / num2;
}

function squareRoot(num) {
    try {
        return Math.sqrt(num);
    } catch (error) {
        console.log(error)
    }
}

function squareArea(side, power = 2) {
    if (power <= 0) {
        return 1;
    }
    return side * squareArea(side, power - 1);
}

const cubeVolume = (side, power = 3) => {
    return power <= 0 ? 1 : side * cubeVolume(side, power - 1);
}

function tubeVolume(base, height) {
    return 1 / 2 * base * height;
}

function getInput() {
    rl.question("What math operation do u want? (+, -, *, /, akar, luas persegi, volume kubus, volume tabung) \n>> ", function(math) {
        if (math === 'luas persegi' || math == 'akar' || math === 'volume kubus') {
            rl.question("Enter the number: ", function(number) {
                switch (math) {
                    case 'akar':
                        result = squareRoot(number);
                        console.log(`Hasil: ${result}`)
                        break;
                    case 'luas persegi':
                        result = squareArea(number);
                        console.log(`Hasil: ${result}`);
                        break;
                    case 'volume kubus':
                        result = cubeVolume(number);
                        console.log(`Hasil: ${result}`);
                        break;
                    default:
                        getInput()
                }
                rl.close();
            });
        }
        rl.question('Enter the first number or base: ', function(num1) {
            rl.question('Enter the second number or height: ', function(num2) {
                switch (math) {
                    case '+':
                        result = sum(Number(num1), Number(num2));
                        console.log(`Hasil: ${result}`);
                        break;
                    case '-':
                        result = substraction(Number(num1), Number(num2));
                        console.log(`Hasil: ${result}`);
                        break;
                    case '*':
                        result = multiple(Number(num1), Number(num2));
                        console.log(`Hasil: ${result}`);
                        break;
                    case '/':
                        result = divided(Number(num1), Number(num2));
                        console.log(`Hasil: ${result}`);
                        break;
                    case 'volume tabung':
                        result = tubeVolume(Number(num1), Number(num2));
                        console.log(`Hasil: ${result}`);
                        break;
                    default:
                        getInput();
                }
                rl.close();
            });
        });
    });
}

getInput();